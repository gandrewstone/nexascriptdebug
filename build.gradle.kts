val ktorVersion: String by project
val kotlinVersion: String by project
val logbackVersion: String by project
// val exposedVersion: String by project
val kotlinxHtmlJvmVersion: String by project
val sqliteJdbcVersion: String by project
val zxingVersion: String by project
val coroutinesVersion: String by project

val bigNumVersion = "0.3.9" // https://github.com/ionspin/kotlin-multiplatform-bignum

plugins {
    application
    kotlin("jvm") version "2.0.21"
    kotlin("plugin.serialization") version "2.0.0"
}

group = "org.nexa.debug"
version = "0.0.3"
application {
    mainClass.set("org.nexa.debug.ApplicationKt")

    val isDevelopment: Boolean = true // project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=true", "-ea")
}

repositories {
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap") }
    maven { url = uri("https://maven.pkg.jetbrains.space/kotlin/p/kotlin/kotlin-js-wrappers") }

    // Nexa repos
    maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpthreads
    maven { url = uri("https://gitlab.com/api/v4/projects/15615113/packages/maven") }  // Wally Enterprise Wallet
    maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") }  // Nexa RPC into nexad
    maven { url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven") }  // Nexa Script Machine
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") }  // LibNexaKotlin
    mavenLocal()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0")
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-webjars-jvm:$ktorVersion")
    implementation("org.webjars:jquery:3.7.1")  // https://github.com/webjars/jquery/tags
    implementation("io.ktor:ktor-server-websockets-jvm:$ktorVersion")
    implementation("org.jetbrains.kotlinx","kotlinx-html-jvm",kotlinxHtmlJvmVersion)
    implementation("io.ktor:ktor-server-html-builder-jvm:$ktorVersion")
    //implementation("org.jetbrains:kotlin-css-jvm:1.0.0-pre.129-kotlin-1.4.20")
    implementation("org.jetbrains.kotlin-wrappers:kotlin-css:1.0.0-pre.754")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-forwarded-header-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-default-headers-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-cors-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-sessions-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-netty-jvm:$ktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.8.0")
    implementation("com.google.zxing:core:$zxingVersion") // QR code creation

    // for bigintegers/bigdecimal
    implementation("com.ionspin.kotlin:bignum:$bigNumVersion")
    implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:$bigNumVersion")


    // Database for block and header storage
    //implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    //implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    //implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    //implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")
    //implementation("org.xerial:sqlite-jdbc:$sqliteJdbcVersion")

    // Nexa
    implementation("org.nexa:mpthreads:0.3.3")  // https://gitlab.com/nexa/mpthreads/-/packages
    implementation("org.nexa:libnexakotlin:0.3.11") // https://gitlab.com/nexa/libnexakotlin/-/packages
    implementation("org.nexa:scriptmachine:1.1.1")
    // implementation("org.wallywallet:wew:1.1.39")

    // TESTING
    testImplementation("io.ktor:ktor-server-test-host:$ktorVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
}

val BASEDIR = "$group-$version"
val BINDIR = BASEDIR + "/bin"

tasks.getByName<Zip>("distZip") {
    /*
    with(copySpec {
        from("jars/libnexajvm.jar").into(BINDIR)
    })
     */
    with(copySpec {
        //from("libnexa.so").into(BINDIR)
    })
}

// Grab my libraries from the jars directory (for local development)
fun DependencyHandlerScope.localOrReleased(localFile: String, dependencyNotation: Any, forceLocal:Boolean? = null)
{
    val fl = if (forceLocal==null) project.property("localJars").toString().trim().toBooleanStrict()
    else forceLocal

    val hasLocal = if (fl)
    {
        val nexaJvm = File(rootProject.rootDir,localFile)
        if (nexaJvm.exists())
        {
            println("Using local library: $localFile")
            implementation(files(nexaJvm))
            true
        }
        else
        {
            println("WARNING: Even though LOCAL_JARS is true $localFile pulled from its released version because it" +
                " does not exist at ${nexaJvm.absolutePath}.")
            false
        }
    } else false

    if (!hasLocal)
    {
        println("Using release library for: $localFile")
        implementation(dependencyNotation)
    }
}

// makes the standard streams (err and out) visible at console when running tests
tasks.withType<Test> {
    testLogging {
        showStandardStreams = true
    }
    outputs.upToDateWhen { false }  // Always rerun test tasks
}