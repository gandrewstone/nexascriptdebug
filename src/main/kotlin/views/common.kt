package org.nexa.debug

import kotlinx.html.*
import java.util.logging.Logger

private val LogIt = Logger.getLogger("ndb.common")

// Add needed fields to the script tag
@HtmlTagMarker
fun mscript(consumer: TagConsumer<*>, type : String? = null, src : String? = null, integrity : String? = null, crossorigin: String? = null, defer: String? = null, referrerPolicy: String? = null) : Unit =
    SCRIPT(attributesMapOf("type", type,"src", src, "integrity", integrity, "crossorgin", crossorigin, "defer", defer, "referrerPolicy", referrerPolicy), consumer).visit({})

@HtmlTagMarker
fun HEAD.ogmeta(property : String, content : String,  block: META.() -> Unit = {}) : Unit
{
    meta()
    {
        attributes["property"] = property
        attributes["content"] = content
        block()
    }
}

fun HTML.ndbHeader(t: String? = null, css:List<String>? = null)
{
    head {
        meta(charset = "utf-8")
        meta(name = "viewport", content = "width=device-width, initial-scale=1")
        //link("https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css", rel="stylesheet")
        // {
        //    attributes["crossorigin"] = "anonymous"
        //}
        link("https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css", rel = "stylesheet")
        {
            integrity = "sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
            attributes["crossorigin"] = "anonymous"
        }
        link("/styles.css", rel = "stylesheet")
        if (css != null) for (c in css)
        {
            link("/s/$c", rel = "stylesheet")
        }
        title(NDB_TITLE + if (t != null) ":" + t else "")
        link(href = "/s/logo.png", rel = "icon", type = "img/png")
        ogmeta("og:title", NDB_TITLE)
        ogmeta("og:description", "Nexa Script Debugger")
        ogmeta("og:site_name", NDB_TITLE)
        ogmeta("og:url", NDB_SERVER_FQDN)
        ogmeta("og:image", NDB_SERVER_FQDN + FSEP + SITE_FLAG_IMG)
        ogmeta("og:type", "website")
    }
}

fun BODY.ndbScripts(js:List<String>? = null)
{
    if (js != null) for (j in js)
        mscript(consumer, type = "application/javascript", STATIC_ROUTE + FSEP + j)

}

fun BODY.ndbPostscripts()
{
    mscript(consumer,type = "application/javascript", src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js",
        // breaks if there's any error! "sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" ,
        crossorigin="anonymous")
    mscript(consumer, type = "application/javascript", src = "/s/postscript.js")
}


fun DIV.ndbTopbar(session: NdbSession, title: String? = null)
{
    nav(classes = "p-1 navbar navbar-expand-lg")
    {
        div(classes = "container-fluid")
        {
            div("d-flex flex-row")
            {
                button(classes = "navbar-toggler", type = ButtonType.button)
                {
                    attributes["data-bs-toggle"] = "collapse"
                    attributes["data-bs-target"] = "#navbarSupportedContent"
                    attributes["aria-controls"] = "navbarSupportedContent"
                    attributes["aria-expanded"] = "false"
                    attributes["aria-label"] = "Toggle navigation"
                    span(classes = "navbar-toggler-icon")
                }
                a(classes = "navbar-brand", href = "/") {
                    img(src = "/s/logo.png", alt = "") { width = "24"; height = "24" }
                    +tr(title)
                }
                //div(classes = "collapse navbar-collapse")
                //{
                //id = "navbarSupportedContent"
                //ul(classes = "navbar-nav me-auto mb-2 mb-lg-0 d-flex flex-row")
                //{
                div("navbar-nav")
                {
                    div("nav-item") { dbgCtrl(session, "<<<", """dbgResetScript("begin")""", tr("Reset Nexa script machine completely (no scripts executed)")) }
                    div("nav-item") { dbgCtrl(session, "<<", """dbgResetScript("postpush")""", tr("Reset Nexa script machine to the beginning of template execution")) }
                    div("nav-item") { dbgCtrl(session, ">>", "dbgGoScript()", tr("Finish executing this script")) }
                    div("nav-item") { dbgCtrl(session, ">", "dbgStep()", tr("execute a single instruction")) }
                    div("nav-item") { dbgCtrl(session, "Delete", "dbgClearScript()", tr("Delete the current script machine. This will clear the current script.")) }

                    if (SHOW_UNIMPLEMENTED)
                    {
                        +" "
                        div(classes = "nav-item p-1")
                        {
                            a(href = "/foundry", classes = "nav-link active")
                            {
                                attributes["aria-current"] = "page"
                                +tr("foundry")
                            }
                        }
                    }
                    div(classes = "nav-item p-1")
                    {
                        ifwallet(session)
                        id = "loginButtonDisconnect"
                        a(classes = "nav-link active")
                        {
                            attributes["data-orig-display"] = "block"
                            attributes["onclick"] = "disconnectWallet()"
                            +tr("disconnect")
                        }
                    }
                }
                if (SHOW_UNIMPLEMENTED)
                {
                    form(classes = "d-flex")
                    {
                        input(classes = "form-control me-3", type = InputType.search)
                        {
                            id = "searchField"
                            attributes["placeholder"] = tr("Search")
                            attributes["aria-label"] = tr("Search")
                        }
                        button(classes = "btn btn-outline-success buttonCust", type = ButtonType.submit) {
                            attributes["onclick"] = """return searchButtonClick(document.getElementById("searchField"))"""
                            +tr("Search")
                        }
                    }
                }
                //}
                span(classes = "navbar-text")
                {
                    id = "feedback"
                }
            }
            div(classes = "positionMe")
            {
                div(classes = "connectButton") { connectButton(session) }
            }
        }
    }

}


fun HTML.ndbBody(session: NdbSession, title: String? = null, js:List<String>? = null, blk: DIV.() -> Unit)
{
    body()
    {
        ndbScripts(js)
        /* div()
        {
            id = "dragsAreHere"
        } */
        // div() { connectModal(session) }
        div(classes = "ndbWholeScreen container g-0 d-flex flex-column") // d-flex flex-column
        {
            div(classes = "p-0")
            {
                id = "ndbTopbar"
                ndbTopbar(session, title)
            }
            /* all these alert boxes are removed in favor of a static one located in the topbar
            div(classes = "p-0")
            {
                val hidden = if (session.errorAlert != null) "" else "hidden"
                div(classes = "alert alert-danger $hidden")
                {
                    attributes["role"] = "alert"
                    id = "ndbErrorAlert"
                    button(type = ButtonType.button, classes = "btn-close")
                    {
                        attributes["data-hide"] = "alert"
                        attributes["aria-label"] = "Close"
                    }
                    span(classes = "align-bottom")
                    {
                        id = "ndbErrorAlertText"
                        + (session.errorAlert ?: "")
                    }
                }
            }
            div(classes = "p-0")
            {
                val hidden = if (session?.warningAlert != null) "" else "hidden"
                div(classes = "alert alert-warning $hidden")
                {
                    attributes["role"] = "alert"
                    id = "ndbWarningAlert"
                    button(type = ButtonType.button, classes = "btn-close")
                    {
                        attributes["data-hide"] = "alert"
                        attributes["aria-label"] = "Close"
                    }
                    span(classes = "align-bottom")
                    {
                        id = "ndbWarningAlertText"
                    }
                }
            }
            div(classes = "p-0")
            {
                val hidden = if (session?.infoAlert != null) "" else "hidden"
                div(classes = "alert alert-primary $hidden")
                {
                    attributes["role"] = "alert"
                    id = "ndbInfoAlert"

                    span(classes = "align-middle")
                    {
                        button(type = ButtonType.button, classes = "btn-close")
                        {
                            attributes["data-hide"] = "alert"
                            attributes["aria-label"] = "Close"
                        }
                    }
                    span(classes = "align-bottom")
                    {
                        id = "ndbInfoAlertText"
                    }
                }
            }
            //div(classes = "row")
            div(classes = "p-0")
            {
                val hidden = if (session?.successAlert != null) "" else "hidden"
                div(classes = "alert alert-success $hidden")
                {
                    attributes["role"] = "alert"
                    id = "ndbSuccessAlert"
                    span(classes = "align-middle")
                    {
                        button(type = ButtonType.button, classes = "btn-close")
                        {
                            attributes["data-hide"] = "alert"
                            attributes["aria-label"] = "Close"
                        }
                    }
                    span(classes = "align-bottom")
                    {
                        id = "ndbSuccessAlertText"
                        session?.successAlert?.let {
                            +it
                        }
                    }
                }
            }
*/
            blk()
        }

        ndbPostscripts()
    }
}



fun DIV.dbgCtrl(session: NdbSession, text:String, onclick:String, tooltip:String)
{
    val sid = session.sessionId
    if (sid == null) return

        form(classes = "buttonForm")
        {
            button(classes = "btn btn-outline-success buttonCust overlapSpaceLoginButton", type = ButtonType.button)
            {
                attributes["data-orig-display"] = "block"
                attributes["onclick"] = onclick
                attributes["data-bs-toggle"] = "tooltip"
                attributes["data-bs-placement"] = "bottom"
                attributes["data-bs-delay"]="""'{"show":2000,"hide":10}'"""
                title = tooltip
                +text
            }
        }
}

fun DIV.connectButton(session: NdbSession?)
{
    if (!SHOW_UNIMPLEMENTED) return
    val sid = session?.sessionId
    if (sid != null)
    {
        form(classes = "loginForm")
        {
            /*  Show a big disconnect button (currently moved to a item in the navbar)
            button(classes = "btn btn-outline-success loginButton buttonCust", type = ButtonType.button)
            {
                ifwallet(session)
                div()
                {
                    ifwallet(session)
                    id = "loginButtonDisconnect"
                    attributes["data-orig-display"] = "block"
                    attributes["onclick"] = "disconnectWallet()"
                    +tr("Disconnect")
                }
            }

             */
            // Connect button
            button(classes = "btn btn-outline-success loginButton buttonCust overlapSpaceLoginButton", type = ButtonType.button)
            {
                ifnotwallet(session)
                div(classes = "btn")
                {
                    ifnotwallet(session)
                    id = "loginButtonConnect"
                    attributes["data-orig-display"] = "block"
                    attributes["onclick"] = "openModalConnect()"
                    +tr("Connect")
                    /*
                    unsafe()
                    {
                        +createQrSvg("longPollQR", "tdpp://" + NDB_SERVER_FQDN + "/lp?cookie=${sid.id}", 80)
                    }

                     */
                }
            }
        }
    }
    else
    {
        // Every call should have a session
    }
}