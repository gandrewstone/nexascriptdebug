package org.nexa.debug

import io.ktor.server.websocket.*
import io.ktor.websocket.*
import kotlin.time.Duration
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlin.time.Duration.Companion.seconds

fun Application.configureSockets() {
    install(WebSockets) {
        pingPeriod = java.time.Duration.ofSeconds(15) // 15.seconds
        timeout = java.time.Duration.ofSeconds(15) // 15.seconds
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }

    routing {
        webSocket("/") { // websocketSession
            for (frame in incoming) {
                when (frame) {
                    is Frame.Text -> {
                        val text = frame.readText()
                        outgoing.send(Frame.Text("YOU SAID: $text"))
                        if (text.equals("bye", ignoreCase = true)) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                        }
                    }
                    is Frame.Binary -> {}
                    is Frame.Close -> {}
                    is Frame.Ping -> {}
                    is Frame.Pong -> {}
                }
            }
        }
    }
}
