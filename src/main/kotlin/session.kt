package org.nexa.debug
//import org.nexa.libnexakotlin.*
import org.nexa.scriptmachine.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.sessions.*
import kotlinx.html.HTMLTag
import java.time.Duration
import java.time.Instant
import java.util.logging.Logger

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import org.nexa.libnexakotlin.NexaTransaction
import org.nexa.libnexakotlin.NexaTxOutput
import org.nexa.libnexakotlin.SatoshiScript
import org.nexa.libnexakotlin.launch

data class NdbSessionId(val id: String)

private val LogIt = Logger.getLogger("ndb.session")

// Can't access ktor sessions given an arbitrary cookie, so I need to make my own.
// Also RAM session cookie never times out so bad for release
data class NdbSession(var sessionId: NdbSessionId? = null,
                      var currentPage: String? = null,
                      var identity: String? = null,  // identity (BCH address) of the session creator, if they logged in via BCHidentity
                      var username: String?=null,
                      val addresses: MutableList<String> = mutableListOf(),
                        //val groups: MutableList<GroupId> = mutableListOf(),
                        //var txProposal:Transaction? = null,
                        //var txContinuation: (suspend (NdbSession, Transaction) -> String)? = null,  // if a tx proposal comes back, call this function with the result of analyzing the returned tx
                        //val assets: MutableMap<GroupId, AssetInfo> = mutableMapOf(),    // NFTs user claims to own
                        //val quickbar: MutableMap<GroupId, AssetInfo> = mutableMapOf(),  // NFTs user is interested in
    // Debugging state
                        // These are possible input data
                      var coins: MutableList<NexaTxOutput> = mutableListOf(),
                      var script: SatoshiScript? = null,
                      var txes: MutableList<NexaTransaction> = mutableListOf(),

                      var sm: ScriptMachine? = null,
    // Alerts
                      var successAlert: String? = null,
                      var errorAlert: String? = null,
                      var warningAlert: String? = null,
                      var infoAlert: String? = null,
    // Client page dynamic updates
                      var lastClientConnection: Instant = Instant.MIN,
                      var longPollHasData: Channel<Boolean> = Channel(Channel.UNLIMITED),
                      var longPoll: MutableList<String> = mutableListOf(),  // Long poll dynamic page changes

    // Wallet communication
                      var activeWalletConnection:Boolean? = null,  // Null means indeterminate state (just restarted)
                      var lastWalletConnection: Instant = Instant.now(),
                      var walletLongPollHasData: Channel<Boolean> = Channel(Channel.UNLIMITED),
                      var walletLongPoll: MutableList<String> = mutableListOf(),  // Long poll dynamic page changes
                        //var chainSelector: ChainSelector = DEFAULT_CHAIN
)
{

    fun clearAlerts()
    {
        // setting to null says "ignore" -- old alerts might hang around.  Setting to "" says get rid of them in the UX.
        // so this is a little bit of a hack because since all alerts lay over eachother I just use successAlert="" to wipe them once.
        // otherwise an error of "" would wipe out a real warning that is added in the same UX update
        successAlert = ""
        infoAlert = null
        warningAlert = null
        errorAlert = null
    }
    // By storing these alerts on the server, the messages continue to appear even if the page moves
    fun setSuccess(s: String, crossPages: Boolean = true)
    {
        successAlert = s
        launch {  // clear it after some time
            delay(ALERT_CLEAR_DELAY)
            if (successAlert == s) successAlert=null
        }
        launch {
            pushToPage("""{ "op": "ex", "name":"successAlert", "arg":"$s" }""")
        }
    }

    fun setInfo(s: String)
    {
        infoAlert = s
        launch {  // clear it after some time
            delay(ALERT_CLEAR_DELAY)
            if (infoAlert == s) infoAlert=null
        }
        launch {
            pushToPage("""{ "op": "ex", "name":"warningAlert", "arg":"$s" }""")
        }
    }

    fun setWarning(s: String)
    {
        warningAlert = s
        launch {  // clear it after some time
            delay(ALERT_CLEAR_DELAY)
            if (warningAlert == s) warningAlert=null
        }
        launch {
            pushToPage("""{ "op": "ex", "name":"warningAlert", "arg":"$s" }""")
        }
    }

    fun setError(s: String? = "unspecified error"): String
    {
        errorAlert = s
        launch {  // clear it after some time
            delay(ALERT_CLEAR_DELAY)
            if (errorAlert == s) errorAlert=null
        }
        return("""{ "error":"$s" }""")
    }

    // Update the UI element to show we don't have a wallet connection
    suspend fun walletDisconnectedUx()
    {
        pushToPage("""{ "op":"vis", "on":"loginButtonConnect ifnotwallet", "off":"loginButtonDisconnect ifwallet"}""")
    }

    // Update the UI element to show we don't have a wallet connection
    suspend fun walletConnectedUx()
    {
        // Workaround: the mobile web server runs off to handle the app link, suspending the browser which can miss this push.  So
        // send it twice after a bit of time
        launch {
            delay(200)
            pushToPage("""{ "op":"vis", "off":"connectQrModal loginButtonConnect ifnotwallet", "on":"loginButtonDisconnect ifwallet"}""")
            delay(500)
            pushToPage("""{ "op":"vis", "off":"connectQrModal loginButtonConnect ifnotwallet", "on":"loginButtonDisconnect ifwallet"}""")
        }
    }

    // When the client contacts us for async communications, we have the opportunity to correct any incorrect state that it might show, so check that state
    fun checkState()
    {
        // If the wallet hasn't checked in awhile, assume that its gone.
        // But it may just be sleeping so don't clear the assets
        checkWalletAlive(this)

        /*  long pool logic handles the reconnection so this is redundant
        else if (!activeWalletConnection && (lastWalletConnection > Instant.now().minusMillis(WALLET_LONG_POLL_TIMEOUT*3)))
        {
            activeWalletConnection = true
            launch { walletConnectedUx() }
        }
         */


    }

    // When there's a new page loaded, clear out any long poll changes being pushed at the old page
    // Returns a function that pushes json data to the new page via long polling.
    fun newPage(): suspend (String) -> Unit
    {
        val tmp = longPollHasData
        longPollHasData = Channel(Channel.UNLIMITED)
        tmp.cancel()

        longPoll.clear()
        longPoll = mutableListOf()

        // take these as temporaries so a subsequent call to newPage does not change them
        val lp = longPoll
        val ch = longPollHasData
        return { json ->
            lp.add(json)
            ch.send(true)
        }
    }

    // Prefer calling function returned as the result of newPage(), since it fails when the client moves to a different page
    suspend fun pushToPage(json: String)
    {
        longPoll.add(json)
        longPollHasData.send(true)
    }

    suspend fun pushToWallet(req: String)
    {
        walletLongPoll.add(req)
        walletLongPollHasData.send(true)
    }
}

val sessions = mutableMapOf<String,NdbSession>()



fun findCreateSession(call: ApplicationCall?): Pair<String,NdbSession>
{
    var nsi: NdbSessionId? = call?.sessions?.get()
    if (nsi == null)
    {
        System.out.println("No session info")
        nsi = NdbSessionId(newCookie(SESSION_ID_LENGTH))
        call?.sessions?.set(nsi)
    }
    var ret = sessions[nsi.id]
    if (ret == null)
    {
        ret = NdbSession(nsi)
        sessions[nsi.id] = ret

    }
    return Pair(nsi.id, ret!!)
}


// Helper function to do everything needed when client moves to a new page
// 1. load/create the session
// 2. reset the long poll data
fun sessionNewPage(call: ApplicationCall?): Triple<String, NdbSession, suspend (String) -> Unit>
{
    val (cookie, session) = findCreateSession(call)
    val pushToPage = session.newPage()
    val pagePath = call?.request?.path()
    session.currentPage = pagePath
    return Triple(cookie, session, pushToPage)
}

fun checkWalletAlive(session: NdbSession?): Boolean
{
    if (session == null) return false
    val now = Instant.now()
    if (session.activeWalletConnection != true) return false
    if (session.lastWalletConnection < now - Duration.ofMillis(WALLET_LONG_POLL_TOLERANCE_TIME + WALLET_LONG_POLL_TIMEOUT)) // wallet didn't call _lp within 15 seconds of the last connection
    {
        LogIt.info("Wallet long poll for session ${session.sessionId?.id ?: "[null]"} expired")
        // Assume that wallet connection is lost.  If it reconnects, it'll autochange the indicator
        session.activeWalletConnection = false
        launch { session.walletDisconnectedUx() }
        return false
    }
    return true
}

// show if the wallet is connected.  Sets the class and the initial style
fun HTMLTag.ifwallet(session: NdbSession?)
{
    checkWalletAlive(session)
    this.attributes["class"] = (this.attributes["class"] ?: "") + " ifwallet"
    if (session?.activeWalletConnection != true)
    {
        attributes["style"] = "display:none"
    }
}

// don't show if the wallet is connected. Sets the class and the initial style
fun HTMLTag.ifnotwallet(session: NdbSession?)
{
    checkWalletAlive(session)
    this.attributes["class"] = (this.attributes["class"] ?: "") + " ifnotwallet"
    if (!(session?.activeWalletConnection != true))
    {
        attributes["style"] = "display:none"
    }
}

// By only using caps, this cookie will work in limited-char QR codes
val cookieChars : List<Char> = ('A'..'Z') + ('0'..'9')

fun newCookie(len: Int=16):String
{
    return (1..len).map({ cookieChars[(0 until cookieChars.size).random()]}).joinToString("")
}