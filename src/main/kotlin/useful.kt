package org.nexa.debug
import org.nexa.libnexakotlin.*
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.ZipInputStream
import com.ionspin.kotlin.bignum.decimal.BigDecimal

const val DEFAULT_CURRENCY_UNIT_NAME = "MEX"
val defaultCurrencyUnitFormat = NexaFormat

fun defaultToFinestCurrencyUnit(x:String): Long = defaultToFinestCurrencyUnit(CurrencyDecimal(x))

// 100 sats in a Nexa
fun defaultToFinestCurrencyUnit(x: BigDecimal) = (x*BigDecimal.fromInt(100)).toLong()
fun defaultToFinestCurrencyUnit(x:Long) = (x*1000*100).toLong()
fun finestToDefaultCurrencyUnit(x:Long): BigDecimal
{
    val ret = CurrencyDecimal(x) / CurrencyDecimal(1000*100)
    return ret
}


// Well its not fenced, not an authority and is a subgroup so pretty good chance of being a nifty
fun GroupInfo?.possibleNfty(): Boolean
{
    if (this == null) return false
    return (!isAuthority() && isSubgroup() && !groupId.isFenced())
}

/* A bytearray with content comparison */
class ByteString(val value: ByteArray)
{
    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ByteString
        return value.contentEquals(other.value)
    }

    override fun hashCode(): Int
    {
        return value.contentHashCode()
    }

    override fun toString(): String
    {
        return value.toHex()
    }
}

fun displayCurrency(x: Long): String
{
    return defaultCurrencyUnitFormat.format(finestToDefaultCurrencyUnit(x)) + " " + DEFAULT_CURRENCY_UNIT_NAME
}

fun SatoshiScript.Companion.Grouped(chainSelector: ChainSelector, groupId: GroupId, groupAmount: Long, rest: SatoshiScript? = null): SatoshiScript
{
    var ret = SatoshiScript(chainSelector, SatoshiScript.Type.TEMPLATE, OP.push(groupId.data), OP.groupAmount(groupAmount))
    if (rest != null) ret = ret + rest
    return ret
}

// Escapes double quotes and backslashes so this string is a valid json string
fun String.jsonString(): String
{
    var s = replace("\\","\\\\")
    s = s.replace("\n"," ")  // new lines are not acceptable in json strings
    return s.replace(""""""","""\"""")
}

fun Map<String, String>.jsonify(): String
{
    val ret = StringBuilder()
    ret.append("{\n")
    var first = true
    for ((k,v) in this)
    {
        if (!first) ret.append(",\n")
        else first = false
        val jk = k.jsonString()
        val vk = v.jsonString()
        ret.append("\"$jk\": \"$vk\"")
    }
    ret.append("}")
    return ret.toString()
}

fun String.ellipsis(maxLen: Int): String
{
    if (maxLen <= 0) return ""
    if (maxLen <= 10) return this.take(maxLen)  // If ellipsis would take up a significant amount of the available space, then don't show it.
    if (this.length > maxLen-3) return this.take(maxLen-3) + "..."
    return this
}

fun String.filenameify(default: String=""): String
{
    if (this == "") return default
    val reg = Regex("\\W+")
    val regWht = Regex("\\s+")
    val s = replace(regWht,"_")
    return s.replace(reg,"")
}

fun unzip(zipFile: File, outDir: File)
{
    if (!outDir.exists())
    {
        outDir.mkdir()
    }
    val zipIn = ZipInputStream(FileInputStream(zipFile))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        val filePath: String = outDir.toString() + FSEP + entry.name
        if (!entry.isDirectory)
        {
            val out = BufferedOutputStream(FileOutputStream(filePath))
            zipIn.copyTo(out)
            out.close()
        }
        else
        {
            val dir = File(filePath)
            dir.mkdirs()
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }

    zipIn.close()
}


fun NexaTransaction.inputAmounts(): Long
{
    var inamt: Long = 0
    for (inp in inputs)
    {
        if (inp.spendable.amount == -1L)
        {
            throw ImplementationException("Call discoverPrevouts before calling additionalAmountRequired");
        }
        assert(inp.spendable.amount > 0)
        inamt += inp.spendable.amount
    }
    // TODO, make a var: inputSatoshis = inamt
    return inamt
}

// Returns the total input quantity - the total output quantity
// throws ImplementationException if the prevouts are not fully "discovered" (call discoverPrevouts).
fun NexaTransaction.amountBalance(): Long
{
    var inamt: Long = 0
    var outamt: Long = 0
    for (inp in inputs)
    {
        if (inp.spendable.amount == -1L)
        {
            throw ImplementationException("Call discoverPrevouts before calling additionalAmountRequired");
        }
        assert(inp.spendable.amount > 0)
        inamt += inp.spendable.amount
    }
    for (out in outputs)
    {
        assert(out.amount > 0)
        outamt += out.amount
    }
    return inamt-outamt
}

