package org.nexa.debug

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.routing.*
import io.ktor.server.webjars.*
import org.nexa.libnexakotlin.ErrorSeverity
import java.util.logging.Logger

private val LogIt = Logger.getLogger("ndb.app")

open class NdbException(msg: String, val severity: ErrorSeverity = ErrorSeverity.Abnormal): Exception(msg)
open class UnavailableException(msg: String): NdbException(msg, ErrorSeverity.Abnormal)
open class TransactionException(msg: String): NdbException(msg, ErrorSeverity.Abnormal)
open class ImplementationException(msg: String): NdbException(msg, ErrorSeverity.Abnormal)


fun main()
{
    parseConfigFile()
    DesktopWallet.init(NDB_WALLET_NAME)

    embeddedServer(Netty, port = 7998, host = "0.0.0.0", watchPaths= listOf("main","resources")) {
        configureSockets()
        configureTemplating()
        configureSerialization()
        configureHTTP()
        configureSecurity()
        configureRouting()
    }.start(wait = true)
}

