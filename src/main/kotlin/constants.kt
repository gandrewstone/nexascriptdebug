package org.nexa.debug
import kotlinx.serialization.Serializable
import org.nexa.libnexakotlin.ChainSelector
import org.nexa.libnexakotlin.chainToURI
import org.nexa.libnexakotlin.uriToChain
import java.io.File

val FSEP = File.separator
val BASE_DIR = System.getProperty("user.dir")

var NDB_TITLE = "NexaDbg"  // Sometimes overridden by the config file (indicates which network), but its ok to use early
const val SHOW_UNIMPLEMENTED = false
const val SESSION_ID_LENGTH = 16
const val ALERT_CLEAR_DELAY = 5000L


// User wallet that might connect in
val LONG_POLL_TIMEOUT = 5000L
val WALLET_LONG_POLL_TIMEOUT = 5000L
val WALLET_LONG_POLL_TOLERANCE_TIME = 10000L  // 5 seconds after timeout to long poll again

// My own wallet
var NDB_WALLET_NAME = "ndbTestnetWallet"


var NDB_CONFIG_FILE = BASE_DIR + FSEP + "ndbcfg.json"
// These are set by the config file so don't use before main()
var NDB_SERVER_FQDN = ""  // get from config file
var DEFAULT_CHAIN = ChainSelector.NEXATESTNET

val STATIC_ROUTE = "/s"

val SITE_FLAG_IMG = STATIC_ROUTE + "/ndbgFlag.png"


val NO_SCRIPT_MACHINE = tr("No Script Machine")


fun parseConfigFile()
{
    val sep = System.getProperty("path.seperator") ?: ":"
    val userDir = System.getProperty("user.dir")
    System.out.println("Current directory: " + userDir)
    // Load the config file into a structure
    @Serializable
    data class ServerConfig(val domainName: String? = null, val walletFile: String? = null, val niftyGroup: String? = null, val blockchain: String? = null)
    @Serializable
    data class ConfigFile(val server: ServerConfig)
    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }
    val cfg = File(NDB_CONFIG_FILE).readText(Charsets.UTF_8)
    val parsed = json.decodeFromString(ConfigFile.serializer(), cfg)

    // Set various globals from the config file structure
    if (parsed.server.domainName != null) NDB_SERVER_FQDN = parsed.server.domainName
    if (parsed.server.walletFile != null) NDB_WALLET_NAME = parsed.server.walletFile
    parsed.server.blockchain?.let {
        DEFAULT_CHAIN = uriToChain[it]!!
    }

    if (DEFAULT_CHAIN != ChainSelector.NEXA) NDB_TITLE = "[" + chainToURI[DEFAULT_CHAIN] + "] " + NDB_TITLE
}