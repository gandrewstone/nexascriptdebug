/*
const triggerTabList = document.querySelectorAll('.nav-tabs button');

triggerTabList.forEach(triggerEl => {
    const tabTrigger = new bootstrap.Tab(triggerEl);
    triggerEl.addEventListener('click', event => {
        console.log("CLICK");
        console.log(triggerEl);
        event.preventDefault();
        console.log(tabTrigger);
        let result = tabTrigger.show();
        console.log(result);
      })
})
*/

// start all tooltips
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
